#include "Primes.h"
using namespace std;

int getPosition(int** factorization , int c){
    int x = 0;
    bool check = true;
    while(check == true){
        if(factorization[c][x] == 1)
            break;
        x++;
    }

    return(x);
}


int*   primeFactors(int n)
{
    int * factors;
    factors = new int[n];
    int c = 0;
    bool isPrime ;
    int last = 1;

    for(int i = n-1; i >1 ; i--)
    {
        isPrime = true;
        if(n%i==0)
        {
            //checks if number is prime by checking if its divisible by any other numbers
            for(int x = 2; x < i ; x++)
            {
                if(i%x == 0)
                    isPrime = false;
            }
            // if it is prime
            if(isPrime == true)
            {
                //adds into the array
                factors[c] = i;
                c++;
                //cout << i << endl;
                // divide the actual number by the prime number and check if it can be used again
                n = n/i;
                i++;
            }
        }
    }
    // adds 1 to mark the end
    factors[c] = last;
    //cout <<  c << endl;
    //cout << factors[c] << endl;
    //Creates smaller Array
    /*
    int * factorsRed ;
    factorsRed = new int[c];

    for( int i = 0 ; i <= c; i ++){
        factorsRed[i] = factors[i];
    }
    */
    return factors;
}

int**  allPrimeFactors(int n)
{
    bool isPrime ;
    int counter = 0;
    int size = n-1;
    int ** allFactors;
    allFactors = new int*[size];

    for(int i = 2 ; i <= n ; i++)
    {
        isPrime = true;
        for(int x = 2; x < i ; x++)
            {
                if(i%x == 0)
                    isPrime = false;using namespace std;
            }

        if(isPrime == false){
            allFactors[counter] = primeFactors(i);
            counter++;
        }

        if(isPrime == true){
            int * factors;
            factors = new int[2];
            factors[0] = i;
            factors[1] = 1;

            allFactors[counter] =  factors;
            //cout<<*allFactors[counter]<<endl;
            //cout<<factors[1]<<endl;
            //cout<<endl;
            counter++;

            //cout<<endl;


        }

    }

    return(allFactors);

}

std::string displayPrimeFactors(int** factorization, int length)
{

    std::string newline = "\n";
    std::string format = " = ";
    std::string zero= "0";
    std::string x= "x";
    std::ostringstream currentInt;
    std::string newint;
    std::string finalString;

    bool print = true;

    int c = 0;
    int r = getPosition(factorization,c);
    //cout<<r<<endl;
    int currentnum = 2;

    if(length>1){
        while(print == true){
        //skip
            if (factorization[c][r] == 1){
                r--;
            }
            if( currentnum == 2){
                currentInt << currentnum;
                newint = currentInt.str();
                finalString.append(zero);
                finalString.append(newint);
                finalString.append(format);
                currentInt.str(" ");
            }

            if( r == 0){
                currentInt << factorization[c][r];
                newint = currentInt.str();
                finalString.append(newint);
                currentInt.str(" ");

                finalString.append(newline);
                currentnum++;

            if (currentnum < 10){
                currentInt << currentnum;
                newint = currentInt.str();
                finalString.append(zero);
                finalString.append(newint);
                finalString.append(format);
                currentInt.str(" ");
                }
            if (currentnum >= 10){
                currentInt << currentnum;
                newint = currentInt.str();
                finalString.append(newint);
                finalString.append(format);
                currentInt.str(" ");
                }
            c++;
            r = getPosition(factorization,c);

        }
            if(factorization[c][r]!=1){
                currentInt << factorization[c][r];
                newint = currentInt.str();
                finalString.append(newint);
                finalString.append(x);
                currentInt.str(" ");
            }
    //break
            if(c >= length - 1)
                break;
            r--;
        }
    }
    /*
    cout<<c<<endl;
    cout<<r<<endl;
    cout<<factorization[c][r]<<endl;
    cout<<factorization[c][r-1]<<endl;
    */
    bool lastCheck = true;
    while(lastCheck == true){
        //cout<<r<<endl;
        if(r < 0){
            break;
        }
        if(factorization[c][r]==1)
            r--;
        if(r == 0){
            currentInt << factorization[c][r];
            newint = currentInt.str();
            finalString.append(newint);
            currentInt.str(" ");
            break;
        }
        currentInt << factorization[c][r];
        newint = currentInt.str();
        finalString.append(newint);
        finalString.append(x);
        currentInt.str(" ");
        r--;


    }



    return(finalString);
}





